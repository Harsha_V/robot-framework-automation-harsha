*** Settings ***
Library  AppiumLibrary  20
Library    String    
Library    OperatingSystem 


Resource          ../../BaseModules/base-setup.robot
Resource          ../../BaseModules/pattern.robot
Resource          ../../BaseModules/menuButton.robot
Resource          ../../BaseModules/login.robot
Resource          ../../BaseModules/logout.robot
Resource          keywords/click-toggle-on-default-card.robot



*** Test Cases ***
Launching FacePay Application     
    Launch FacePay Application
    

Login shall Pass
    Login Should Pass
    
    
Swipe pattern lock 
    Swipe pattern lock and continue  
        

Click on cards management
    Cards Management Button
    

Select the default card
    Click on default card
    
Click on Toggle Button
    Click on toggle button
    
Click on Save Button
    Click on save button
    

Navigate to Card Management Screen
    Back to Card Management Screen


Back to Home Screen
    Goto home screen
    
    
Logout 
    Logout of the application

