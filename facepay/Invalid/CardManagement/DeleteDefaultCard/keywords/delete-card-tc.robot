*** Settings ***
Library  AppiumLibrary  20
Library    String    
Library    OperatingSystem 

Variables         ../../../var/variables.py
Resource          ../../../BaseModules/menuButton.robot


*** Keywords ***
    
Click on default card    
    Click Element    xpath=//*[@text=\"Default\"]
    Wait Until Page Contains      Edit Card 
    Click Element    xpath=//*[@text=\"Delete Card\"]
    
    
Back to Card Management Screen
    Click Element    xpath=//*[@class='android.widget.ImageButton'][@index=0]
    Wait Until Page Contains     Cards management 
    
    
Goto home screen
    Back to Home Screen

