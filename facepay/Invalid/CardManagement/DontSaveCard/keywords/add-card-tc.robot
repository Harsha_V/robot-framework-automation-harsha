*** Settings ***
Library  AppiumLibrary  20
Library    String    
Library    OperatingSystem 

Variables         ../../../var/variables.py
Resource          ../../../BaseModules/menuButton.robot


*** Keywords ***

Add Card    
    Click Element    xpath=//*[@text=\"Add Card\"]
    Wait Until Page Contains      Card   
    
    Input Text       xpath=//*[@text=\"424242424242\"]           ${InvalidCardNumber}
    Sleep    1s 
    Input Text       xpath=//*[@text=\"04/24\"]                  ${InvalidCardValidity}
    Sleep    1s
    Input Text       xpath=//*[@text=\"123\"]                    ${InvalidCardCVV}
    Sleep    1s 
    
    
Back to Card Management Screen
    Click Element    xpath=//*[@class='android.widget.ImageButton'][@index=0]
    Wait Until Page Contains     Cards management 
    
Goto home screen
    Back to Home Screen