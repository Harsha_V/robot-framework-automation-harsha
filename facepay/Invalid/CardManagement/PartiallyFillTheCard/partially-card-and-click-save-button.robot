*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 


Resource          ../../BaseModules/base-setup.robot
Resource          ../../BaseModules/pattern.robot
Resource          ../../BaseModules/menuButton.robot
Resource          ../../BaseModules/login.robot
Resource          ../../BaseModules/logout.robot
Resource          keywords/partial-card-tc.robot



*** Test Cases ***
Launching FacePay Application     
    Launch FacePay Application
    

Login shall Pass
    Login Should Pass
    
    
Swipe pattern lock 
    Swipe pattern lock and continue  
        

Click on cards management
    Cards Management Button
    

Add new card to card management
    Add Card

Click on save butoon
    Save Button
    
Navigate to Card Management Screen
    Back to Card Management Screen

Navigate back to Home Screen
     Back to Home Screen
    
    
Logout 
    Logout of the application

