*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Variables         ../../../var/variables.py
Resource          ../../../BaseModules/menuButton.robot


*** Keywords ***

Add Card    
    Click Element    xpath=//*[@text=\"Add Card\"]
    Wait Until Page Contains      Card   
    
    Input Text       xpath=//*[@text=\"424242424242\"]           ${CardNumber}
    Sleep    1s 
    
    
Save Button    
    Click Element    xpath=//*[@text=\"Save\"]
    
    
Back to Card Management Screen
    Click Element    xpath=//*[@class='android.widget.ImageButton'][@index=0]
    Wait Until Page Contains     Cards management 
    
    
Logout 
    Logout of the application