*** Settings ***
Library  AppiumLibrary  20
Library    String    
Library    OperatingSystem 

Resource          ../../BaseModules/base-setup.robot
Resource          ../../BaseModules/pattern.robot
Resource          ../../BaseModules/menuButton.robot
Resource          ../../BaseModules/login.robot
Resource          ../../BaseModules/logout.robot
Resource          keywords/ptl-tc.robot


*** Test Cases ***
Launching FacePay Application     
    Launch FacePay Application
    

Login shall Pass
    Login Should Pass
    
    
Swipe pattern lock 
    Swipe pattern lock and continue  
    
 
Click on Accounts management
    Account Button
        
    
Per Transaction Limit Negative Cases
    Click on PTL and cancel Btn 
    PTL less than 0.5 cents 
    
click on cancel button
    PTL cancel button

    
    
Back to Home Screen
     Goto home screen
    
    
Logout   
    Logout of the application


