*** Settings ***
Library  AppiumLibrary  20
Library    String    
Library    OperatingSystem 

Variables         ../../../var/variables.py
Resource          ../../../BaseModules/menuButton.robot

*** Keywords ***

Click on PTL and cancel Btn 
    Sleep            2s
    Click Element    xpath=//*[@text=\"Change\"]
    Sleep            2s
    Click Element    xpath=//*[@text=\"CANCEL\"]   
    Sleep            2s

PTL less than 0.5 cents 
    Click Element    xpath=//*[@text=\"Change\"]
    Sleep            2s
    Input Text       xpath=//*[@class='android.widget.EditText'][@index=1]        ${wrongLimit1}
    Sleep            2s
    Click Element    xpath=//*[@text=\"SET\"]
    Sleep            2s
    
PTL cancel button
    Click Element    xpath=//*[@text=\"CANCEL\"]   
    Sleep            2s
    
    
Goto home screen
    Back to Home Screen