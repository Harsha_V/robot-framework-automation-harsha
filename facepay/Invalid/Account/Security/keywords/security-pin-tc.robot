*** Settings ***
Library  AppiumLibrary  20
Library    String    
Library    OperatingSystem 

Variables         ../../../var/variables.py
Resource          ../../../BaseModules/menuButton.robot



*** Keywords ***

Click on change pin button
    sleep    5s
    Click Element    xpath=//*[@text=\"Change PIN\"]
    
    
Wrong current pin test
    Input Text   accessibility_id=update_pin_current            ${wrongPIN}
    Input Text   accessibility_id=update_pin_new                ${newPIN}
    Click Element    xpath=//*[@text=\"Save Changes\"]
    Sleep            2s
    Wait Until Page Contains      Change PIN 
    
Go back to account screen    
    sleep    3s 
    Click Element    xpath=//*[@class='android.widget.ImageButton'][@index=0]


Goto home screen
    sleep    1s
    Back to Home Screen
    
    
Logout   
    Logout of the application