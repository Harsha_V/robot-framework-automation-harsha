*** Settings ***
Library  AppiumLibrary  20
Library    String    
Library    OperatingSystem 


Resource          ../../BaseModules/menuButton.robot
Resource          ../../BaseModules/logout.robot
Resource          ../../BaseModules/facePay_Subscriber.robot
Resource          keywords/security-pin-tc.robot


*** Test Cases ***
Open Application and Login
    Open And Login
    
 
Click on Accounts management
    Account Button
      
            
Test change pin 
    Click on change pin button
    Wrong current pin test


Navigate back to Account Screen
    Go back to account screen
 
    
Navigate back to Home Screen
    Goto home screen
    
    
Logout of the Application    
    Logout
