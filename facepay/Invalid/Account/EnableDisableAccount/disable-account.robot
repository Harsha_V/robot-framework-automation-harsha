*** Settings ***
Library  AppiumLibrary  20
Library    String    
Library    OperatingSystem 

Resource          ../../BaseModules/base-setup.robot
Resource          ../../BaseModules/pattern.robot
Resource          ../../BaseModules/menuButton.robot
Resource          ../../BaseModules/login.robot
Resource          ../../BaseModules/logout.robot
Resource          keywords/disable-account-tc.robot

*** Test Cases ***
Launching FacePay Application     
    Launch FacePay Application
    

Login shall Pass
    Login Should Pass
    
    
Swipe pattern lock 
    Swipe pattern lock and continue  
    
        
click on Account Button
    Account Button


Disable Account
    Disable with disconfirmation
    Click on No button
   
Navigate back to Home Screen
    Goto home screen   

    
Logout of the Application  
    Logout 


