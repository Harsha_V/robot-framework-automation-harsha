*** Settings ***
Library  AppiumLibrary  20
Library    String    
Library    OperatingSystem 

Variables         ../../../var/variables.py



*** Keywords ***
Disable with disconfirmation
    # click on disable account
    sleep    2s
    Click Element     xpath=//*[@text=\"Disable account\"]
    sleep    1s
    
Click on No button
    Click Element     xpath=//*[@text=\"NO\"]
    sleep    1s

    
Goto home screen
      Back to Home Screen
      
      
Logout   
    Logout of the application