*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Variables         ../../../var/variables.py



*** Keywords ***
ForgotPin Button
    Sleep    3s
    Click Element     xpath=//*[@text=\"Forgot PIN\"]
    Wait Until Page Contains    Verify OTP

    
Incorrect OTP less than 3 digits
    Input Text   accessibility_id=verify_code_enter_otp            ${CardCVV}
    
    
Incorrect OTP more than 4 digits
    Clear Text  accessibility_id=verify_code_enter_otp
    Input Text   accessibility_id=verify_code_enter_otp            ${WrongPassword}
    
New Pin          
    Input Text   accessibility_id=verify_code_new_pin            ${newPIN}
    
    
Save BUtton
    Click Element    xpath=//*[@text=\"Save\"]
    Sleep    5s
    
Account Screen
    Click Element    xpath=//*[@class='android.widget.ImageButton'][@index=0]
    Wait Until Page Contains      Account
    
    
    
    
    
    
    
    
    