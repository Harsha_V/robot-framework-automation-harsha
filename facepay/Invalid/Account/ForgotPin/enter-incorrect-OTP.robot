*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Resource          ../../BaseModules/base-setup.robot
Resource          ../../BaseModules/pattern.robot
Resource          ../../BaseModules/menuButton.robot
Resource          ../../BaseModules/login.robot
Resource          ../../BaseModules/logout.robot
Resource          keywords/forgot-pin.robot

*** Test Cases ***
Launching FacePay Application     
    Launch FacePay Application
    

Login shall Pass
    Login Should Pass
    
Swipe pattern lock 
    Swipe pattern lock and continue  
    
        
Click on Account Button
    Account Button
    
Click on Forgot Password
    ForgotPin Button
    
Enter Incorrect OTP less than 3 digits
    Incorrect OTP less than 3 digits
       
Enter new Pin 
    New Pin
    
Click on Save Button
    Save BUtton
    
Enter Incorrect OTP more than 4 digits
    Incorrect OTP more than 4 digits

Click on Save Button again
    Save BUtton    
    
Navigate Back to Account Screen 
    Account Screen
    
Navigate Back to Home Screen 
    Back to Home Screen
   
Logout 
    Logout of the application

    