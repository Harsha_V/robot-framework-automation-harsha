*** Settings ***
Library  AppiumLibrary  20
Library    String    
Library    OperatingSystem 

Variables         ../../../var/variables.py
Resource          ../../../BaseModules/menuButton.robot


*** Keywords ***
    
Pattern heading
    Sleep    1s
    ${heading1}     Get Text    xpath=//*[@text=\"Please enter your existing pattern\"]   
    Should Not Be Equal     ${heading1}        ${ChangePatternHeading1}    
    Sleep    2s
   
   
Invalid pattern for two dots
    Swipe     150     745     400      800
    Sleep    2s
    
    Sleep    1s
    ${threeDotsError}     Get Text    xpath=//*[@text=\"Connect at least 3 dots. Try again\"]   
    Should Not Be Equal     ${threeDotsError}        ${threeDots}    
    Sleep    1s
    
    
Continue button 
    Click Element    xpath=//*[@text=\"Continue\"]
    
    
Clear button 
    Click Element    xpath=//*[@text=\"Clear\"]
    
    Sleep    1s
    ${clearpatternHeading}     Get Text    xpath=//*[@text=\"Draw an unlock Pattern.\"]   
    Should Not Be Equal     ${clearpatternHeading}        ${patternTitle}    
    Sleep    1s
   
