*** Settings ***
Library  AppiumLibrary  20
Library    String    
Library    OperatingSystem 


Resource          ../../BaseModules/base-setup.robot
Resource          ../../BaseModules/pattern.robot
Resource          ../../BaseModules/menuButton.robot
Resource          ../../BaseModules/login.robot
Resource          ../../BaseModules/logout.robot
Resource          keywords/two-dots-pattern-tc.robot



*** Test Cases ***
Launching FacePay Application     
    Launch FacePay Application
    

Login shall Pass
    Login Should Pass
    
    
Swipe pattern lock 
    Swipe pattern lock and continue  
    

Click on change pattern
    Change Pattern Button
    
Verify Change Pattern Screen Heading
    Pattern heading 
    
Swipe Pattern For Two Dots
    Invalid pattern for two dots
    
Click on Continue Button
    Continue button
    
Click on Clear Button
    Clear button
    
    
Goto home screen  
    Back to Home Screen
      
    
Logout 
    Logout of the application
    
    
    
    
    
    
