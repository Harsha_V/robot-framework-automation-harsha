*** Settings ***
Library  AppiumLibrary  20
Library    String    
Library    OperatingSystem 


Resource          ../../BaseModules/base-setup.robot
Resource          ../../BaseModules/pattern.robot
Resource          ../../BaseModules/menuButton.robot
Resource          ../../BaseModules/login.robot
Resource          ../../BaseModules/logout.robot
Resource          keywords/old-pattern.robot



*** Test Cases ***
Launching FacePay Application     
    Launch FacePay Application
    

 Login shall Pass
    Login Should Pass
    
    
Swipe pattern lock 
    Swipe pattern lock and continue  
    

Click on change pattern
    Change Pattern Button
    
Verify Change Pattern Screen Heading
    Pattern heading 
    
draw old pattern
    Old pattern
    
    
draw New Pattern Lock Again
    Draw new pattern lock
    
Click on clear Button
    Clear button
    

Goto home screen
     Back to Home Screen
    
Logout 
    Logout of the application
    
    
    
    
    
    
