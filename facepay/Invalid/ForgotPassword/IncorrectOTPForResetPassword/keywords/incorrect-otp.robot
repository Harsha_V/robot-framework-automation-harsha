*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Variables         ../../../var/variables.py



*** Keywords ***
ForgotPin Button
    Sleep    3s
    Click Element     xpath=//*[@text=\"Forgot password\"]
    Wait Until Page Contains    Forgot password
    
Email Address
    Input Text   accessibility_id=forgotPassword_email            ${Email} 
    
    
Reset Password Button
    Click Element     xpath=//*[@text=\"Reset Password\"]
    Wait Until Page Contains    Reset Password
    Sleep    2s
    
    
Incorrect OTP less than 3 digits
    Input Text   accessibility_id=reset_otp_code            ${CardCVV}
    
    
Incorrect OTP more than 4 digits
    Clear Text  accessibility_id=reset_otp_code
    Input Text   accessibility_id=reset_otp_code            ${WrongPassword}
    
New Password          
    Input Text   accessibility_id=reset_password            ${NewPwd}
    
Confirm New Password          
    Input Text   accessibility_id=reset_confirmPassword            ${NewPwd}
    
    
Save BUtton
    Click Element    xpath=//*[@text=\"Save password\"]
    Sleep    3s
    
    
Forgot Password Screen
    Click Element    xpath=//*[@class='android.widget.ImageButton'][@index=0]
    Wait Until Page Contains      Forgot password
    
Login Screen   
    Click Element    xpath=//*[@class='android.widget.ImageButton'][@index=0]
    Wait Until Page Contains       Welcome to FacePay!
    
    
    
    
    
    