*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Resource          ../../BaseModules/base-setup.robot
Resource          ../../BaseModules/pattern.robot
Resource          ../../BaseModules/menuButton.robot
Resource          ../../BaseModules/login.robot
Resource          ../../BaseModules/logout.robot
Resource          keywords/incorrect-otp.robot

*** Test Cases ***
Launching FacePay Application     
    Launch FacePay Application
    
Click on forgot password
    ForgotPin Button
     
  
Enter Email Address
    Email Address
        
    
Click on Reset Password
    Reset Password Button
    
Enter Incorrect OTP less than 3 digits
    Incorrect OTP less than 3 digits
       
Enter new password 
    New Password 
    
Enter confirm new password 
    Confirm New Password
    
Click on Save Password Button
    Save BUtton
    
    
Enter Incorrect OTP more than 4 digits
    Incorrect OTP more than 4 digits

Click on Save Button again
    Save BUtton    
    
Navigate Back to Forgot Password Screen
    Forgot Password Screen
    
Navigate Back to Login Screen 
    Login Screen 
   

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    