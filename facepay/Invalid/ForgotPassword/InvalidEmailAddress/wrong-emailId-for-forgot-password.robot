*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Resource          ../../BaseModules/base-setup.robot
Resource          ../../BaseModules/pattern.robot
Resource          ../../BaseModules/menuButton.robot
Resource          ../../BaseModules/login.robot
Resource          ../../BaseModules/logout.robot
Resource          keywords/invalid-emailid.robot

*** Test Cases ***
Launching FacePay Application     
    Launch FacePay Application
    
Click on forgot password
    ForgotPin Button
     
  
Enter Email Address
    Email Address
        
    
Click on Reset Password
    Reset Password Button
    
    
Navigate Back to Login Screen 
    Login Screen     