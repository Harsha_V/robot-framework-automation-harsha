*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Variables         ../../../var/variables.py



*** Keywords ***
ForgotPin Button
    Sleep    3s
    Click Element     xpath=//*[@text=\"Forgot password\"]
    Wait Until Page Contains    Forgot password
    
Email Address
    Input Text   accessibility_id=forgotPassword_email            ${InEmail} 
    
    
Reset Password Button
    Click Element     xpath=//*[@text=\"Reset Password\"]
    Wait Until Page Contains    Reset Password
    Sleep    5s
    
Login Screen   
    Click Element    xpath=//*[@class='android.widget.ImageButton'][@index=0]
    Wait Until Page Contains       Welcome to FacePay!