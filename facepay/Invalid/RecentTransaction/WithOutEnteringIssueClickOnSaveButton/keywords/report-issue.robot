*** Settings ***
Library  AppiumLibrary  20
Library    String    
Library    OperatingSystem 

Variables         ../../../var/variables.py



*** Keywords ***
click on the transaction in recent transaction
    Click Element     xpath=//*[@text=\"Inqude LLC\"]
    Sleep    3s
    
Report issue button
    Click Element     xpath=//*[@text=\"Report issue\"]
    Sleep    1s
    
    
Click on options
    Click Element     xpath=//*[@text=\"I didn't do the payment\"]
    Sleep    1s
    
Select I was charged more 
    Click Element     xpath=//*[@text=\"I was charged more, Please refund\"]
    Sleep    1s
    
Report button
    Click Element    xpath=//*[@text=\"Report\"]        
    Sleep    1s
    
Back to Transaction Details Screen
    Click Element    xpath=//*[@class='android.widget.ImageButton'][@index=0]
    Wait Until Page Contains     Transaction Details     
    
    
Goto home screen
      Back to Home Screen