*** Settings ***
Library  AppiumLibrary  20
Library    String    
Library    OperatingSystem 


Resource          ../../BaseModules/base-setup.robot
Resource          ../../BaseModules/pattern.robot
Resource          ../../BaseModules/menuButton.robot
Resource          ../../BaseModules/login.robot
Resource          ../../BaseModules/logout.robot
Resource          keywords/report-issue.robot




*** Test Cases ***
Launching FacePay Application     
    Launch FacePay Application
    

Login shall Pass
    Login Should Pass
    
    
Swipe pattern lock 
    Swipe pattern lock and continue  
 
 
verify recent transactions details 
    click on the transaction in recent transaction
    
    
Click on report issue button
    Report issue button


click on dropdown
    Click on options

    
select another option
    Select I was charged more

    
click on Report Button
    Report button

    
Navigate back to Transaction Detail screen
    Back to Transaction Details Screen


Navigate back to home screen 
    Goto home screen

    
Logout  
    Logout of the application

