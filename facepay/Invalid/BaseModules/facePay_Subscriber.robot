*** Settings ***
Library    AppiumLibrary    20
Library    String    
Library    OperatingSystem 

Variables    ../var/variables.py
Resource     base-setup.robot
Resource     pattern.robot
Resource     login.robot


*** Keywords ***


Open And Login
    Launch FacePay Application
    Login Should Pass
    Swipe pattern lock and continue
    