*** Settings ***
Library  AppiumLibrary  20
Library    String    
Library    OperatingSystem 



*** Keywords ***

Account Button
    Click Element    accessibility_id=menu_button
    Wait Until Page Contains    Account
    
    Click Element    xpath=//*[@text=\"Account\"]
    Wait Until Page Contains     Account
    
Cards Management Button
    Click Element    accessibility_id=menu_button
    Wait Until Page Contains    Cards Management
    
    Click Element    xpath=//*[@text=\"Cards Management\"]
    Wait Until Page Contains     Cards management 
    
Support & Help Button
    
    Click Element    accessibility_id=menu_button
    sleep    2s
    Click Element    xpath=//*[@text=\"Support & Help\"]
    
Change Pattern Button
    Click Element    accessibility_id=menu_button
    Wait Until Page Contains    Change Pattern
    
    Click Element    xpath=//*[@text=\"Change Pattern\"]
    Wait Until Page Contains     Change Pattern
    
SignOut Button
    Click Element    accessibility_id=menu_button
    Wait Until Page Contains    Sign Out
    Sleep    1s 
    
    Click Element    xpath=//*[@text=\"Sign Out\"]
    Wait Until Page Contains      Welcome to FacePay!  
    
    
    
    
    
Back to Home Screen
    Click Element    xpath=//*[@class='android.widget.ImageButton'][@index=0]
    Wait Until Page Contains      Welcome
    
    
    
    
    
    
    
    