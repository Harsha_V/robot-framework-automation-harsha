*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Variables         ../../../var/variables.py



*** Keywords ***
ForgotPin Button
    Sleep    3s
    Click Element     xpath=//*[@text=\"Forgot PIN\"]
    Wait Until Page Contains    Verify OTP
    
    
Resend OTP
    Click Element     xpath=//*[@text=\"Resend OTP\"]
    Sleep    8s
    
put FacePay application in Background
    Background App    3

    
Zoho mail client application
    Start Activity    com.zoho.mail    com.zoho.mail.android.activities.ZMailActivity
    Sleep    45s
    
Enter OTP received from Email
    Wait Until Page Contains    FacePay
    ${subjectLine}     Get Text    id=com.zoho.mail:id/subject
    Log     ${subjectLine}    
    
    # Verify OTP received
    Should Contain    ${subjectLine}    Facepay app
    
    # otp from email
    @{otp}     Split String   ${subjectLine}     
    Log    @{otp}[3] 
    
    # Switch back to FacePay app
    Press Keycode    187
    Click Element    accessibility_id=FacePay    
     
    Sleep    2s
    
    # Verify switched to Facepay app
    Page Should Contain Text    Verify OTP
    
    #Enter the OTP
    Input Text   accessibility_id=verify_code_enter_otp            @{otp}[3]
    
    
New Pin          
    Input Text   accessibility_id=verify_code_new_pin            ${newPIN}
    
    
Old Pin          
    Input Text   accessibility_id=verify_code_new_pin            ${currentPIN}
    
    
Save BUtton
    Click Element    xpath=//*[@text=\"Save\"]
    Sleep    10s
    Page Should Contain Text    Unlock Pattern   
    
    
    
    
    
    
    
    
    
    