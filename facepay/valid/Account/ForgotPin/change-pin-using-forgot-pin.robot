*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Resource          ../../BaseModules/base-setup.robot
Resource          ../../BaseModules/pattern.robot
Resource          ../../BaseModules/menuButton.robot
Resource          ../../BaseModules/login.robot
Resource          ../../BaseModules/logout.robot
Resource          keywords/forgot-pin.robot

*** Test Cases ***
Launching FacePay Application     
    Launch FacePay Application
    

Login shall Pass
    Login Should Pass
    
    
Swipe pattern lock 
    Swipe pattern lock and continue  
    
        
Click on Account Button
    Account Button
    
Click on Forgot Password
    ForgotPin Button
    
Run FacePay application in Background
    put FacePay application in Background

    
Open Zoho mail client Application
    Zoho mail client application  
    
Read and Enter the OTP received from the registered Email id
    Enter OTP received from Email
    
Enter new Pin 
    New Pin
    
Click on Save Button
    Save BUtton
        
Draw unlock pattern 
    Forgot Password swipe pattern lock
    
    
    
Click on Account Button
    Account Button
    
Click on Forgot Password
    ForgotPin Button
    
Run FacePay application in Background
    put FacePay application in Background

    
Open Zoho mail client Application
    Zoho mail client application  
    
Read and Enter the OTP received from the registered Email id
    Enter OTP received from Email
    
Enter the old pin 
    Old Pin
    
Click on Save Button
    Save BUtton
        
Draw unlock pattern 
    Forgot Password swipe pattern lock    
    
Logout 
    Logout of the application

    