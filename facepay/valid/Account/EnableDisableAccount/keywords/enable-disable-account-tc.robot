*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Variables         ../../../var/variables.py



*** Keywords ***
Test Disable Account
    # Disable account with Yes Button
    # click on disable account
    sleep    2s
    Click Element     xpath=//*[@text=\"Disable account\"]
    sleep    1s
    
    # confirm disable option
    Click Element     xpath=//*[@text=\"YES\"]
    sleep    10s    
    
    
    
Test Enable Account
    # enable account with Yes Button
    # click on enable account
    sleep    2s
    Click Element     xpath=//*[@text=\"Enable account\"]
    sleep    1s
    
    # confirm enable option
    Click Element     xpath=//*[@text=\"YES\"]
    sleep    8s   


Goto home screen
      Back to Home Screen
      
      
Logout   
    Logout of the application
