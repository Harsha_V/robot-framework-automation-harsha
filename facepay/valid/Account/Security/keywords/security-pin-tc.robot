*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Variables         ../../../var/variables.py
Resource          ../../../BaseModules/menuButton.robot



*** Keywords ***

Click on change pin button
    Sleep    2s
    Click Element    xpath=//*[@text=\"Change PIN\"]
  
Update pin with new pin
    Clear Text   accessibility_id=update_pin_current 
    Input Text   accessibility_id=update_pin_current            ${currentPIN}
    Input Text   accessibility_id=update_pin_new                ${newPIN}
    Click Element    xpath=//*[@text=\"Save Changes\"]
    Sleep            5s
    
    
Replace with to old pin 
    Clear Text   accessibility_id=update_pin_current 
    Input Text   accessibility_id=update_pin_current            ${newPIN}
    Clear Text   accessibility_id=update_pin_new
    Input Text   accessibility_id=update_pin_new                ${currentPIN}
    Click Element    xpath=//*[@text=\"Save Changes\"]
    Sleep            5s


Goto home screen
    Back to Home Screen
