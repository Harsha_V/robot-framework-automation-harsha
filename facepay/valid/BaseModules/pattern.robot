*** Settings ***
Library  AppiumLibrary  20
Library    String    
Library    OperatingSystem 



*** Keywords ***

Swipe pattern lock and clear
    Wait Until Page Contains    ${FirstPatternText}
    Swipe     155     418     620      400
    
    Wait Until Page Contains    ${SecondPatternText}
    Click Element    xpath=//*[@text=\"Clear\"]  
    Wait Until Page Contains    ${FirstPatternText}
    

Swipe pattern lock and continue
    Wait Until Page Contains    ${FirstPatternText}
    Swipe     155     418     620      400
    Wait Until Page Contains    ${SecondPatternText}
    Swipe     155     418     620      400

    Click Element    xpath=//*[@text=\"Continue\"]  
    
    sleep    5s
    Wait Until Page Contains    ${welcomeScreenRT}
    
    
    
Forgot Password swipe pattern lock
    Swipe     150     535     620      550
    sleep    1s
    Click Element    xpath=//*[@text=\"Continue\"]
    sleep    5s
    Wait Until Page Contains    ${welcomeScreenRT}
    
    

