*** Settings ***
Library    AppiumLibrary    10
Library    String    
Library    OperatingSystem 
Variables    ../var/variables.py


*** Keywords ***


Login Should Fail
      
      Sleep    2s
      Page Should Contain Text   ${LoginTitle}
    
      Input Text   accessibility_id=email           ${WrongEmail}
      Input Text   accessibility_id=password        ${WrongPassword}
      Sleep    2s
      
      Click Element    accessibility_id=login
      Sleep    2s 
      
      ${toast}     Get Text    accessibility_id=toast 
        
      Should Not Be Equal    ${toast}    None    
      Log    ${toast}  
      Sleep    2s
      
   
   
Login Should Pass
    
      Sleep    5s    
      Page Should Contain Text    ${LoginTitle}
    
      Input Text   accessibility_id=login_email           ${Email}
      Input Text   accessibility_id=login_password        ${Password}
      Sleep    2s
      Click Element    accessibility_id=login
      Sleep    7s 
      
      Wait Until Page Contains    ${patternTitle}
      

 
    

   
   