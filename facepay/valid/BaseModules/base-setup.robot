*** Settings ***
Library    AppiumLibrary    10
Library    String    
Library    OperatingSystem 
Variables    ../var/variables.py


*** Keywords ***

Relaunch Application
    Close Application
    Sleep    2s    
    Launch FacePay Application
        



# FacePay  Application
Launch FacePay Application
   Open Application    http://127.0.1.1:4723/wd/hub    platformName=Android    platformVersion=7.0    deviceName=ZW2222ZVCT    appPackage=com.rncameraexample    appActivity=com.rncameraexample.MainActivity
   Sleep    5s

   
   
   
# SMS App
Open SMS
    Open Application    http://127.0.1.1:4723/wd/hub    platformName=Android    platformVersion=7.0    deviceName=ZW2222ZVCT    appPackage=com.google.android.apps.messaging    appActivity=com.google.android.apps.messaging.ui.convertionlist.conversationListActivity     
    Sleep    5s

   
   
   
# gmail App   
Open Gmail
    Start Activity    com.google.android.gm    ConversationListActivityGmail
    Sleep    5s
    