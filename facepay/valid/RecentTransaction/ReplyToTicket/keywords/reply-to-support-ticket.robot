*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Variables         ../../../var/variables.py
Resource          ../../../BaseModules/menuButton.robot



*** Keywords ***

Click on Support Ticket  
    Sleep    2s
    Click Element    xpath=//*[@class=\'android.view.ViewGroup\'][@index=1]   
    Sleep    2s
       
   
Reply to the issue
   Input Text    xpath=//*[@text=\"Reply back\"]        ${issuereply}  
   Sleep    2s  
   Click Element    xpath=//*[@text=\"Send\"]
   Sleep    2s 
    
    
Navigate back to support and help screen 
    Click Element    xpath=//*[@class='android.widget.ImageButton'][@index=0] 
    Sleep    3s
 
Navigate back to home screen   
    Click Element    xpath=//*[@class='android.widget.ImageButton'][@index=0]      
    Sleep    1s

