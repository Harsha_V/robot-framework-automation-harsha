*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Variables         ../../../var/variables.py



*** Keywords ***
Click on the transaction in recent transaction
    Click Element     xpath=//*[@text=\"Inqude LLC\"]
    Sleep    3s
    
Transaction details screen verification
    # Page title Verification
    Page Should Contain Text       ${TDPageTitle} 
    
    # Transaction shop name is displaying
    ${transactionShopName}=  Run Keyword And Return Status    Element Should Be Visible   xpath=//*[@text=\"Inqude LLC\"]
    Log     ${transactionShopName}
    
    # Transaction shop icon is displaying
    ${transactionShopIcon}=  Run Keyword And Return Status    Element Should Be Visible   xpath=//*[@class='android.widget.ImageView'][@index=0] 
    Log     ${transactionShopIcon}    
    
    # Transaction time is displaying
    ${transactionTime}=  Run Keyword And Return Status    Element Should Be Visible   xpath=//*[@class='android.widget.TextView'][@index=2] 
    Log     ${transactionTime}    
    
    # Transaction price is displaying
    ${transactionTime}=  Run Keyword And Return Status    Element Should Be Visible   xpath=//*[@class='android.widget.TextView'][@index=3] 
    Log     ${transactionTime}  
    
    
Goto home screen
      Back to Home Screen