*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 


Resource          ../../BaseModules/base-setup.robot
Resource          ../../BaseModules/pattern.robot
Resource          ../../BaseModules/menuButton.robot
Resource          ../../BaseModules/login.robot
Resource          ../../BaseModules/logout.robot
Resource          keywords/rise-ticket-about-recent-transaction.robot




*** Test Cases ***
Launching FacePay Application     
    Launch FacePay Application
    

Login shall Pass
    Login Should Pass
    
    
Swipe pattern lock 
    Swipe pattern lock and continue  
 
 
Rise ticket from transaction details 
     Click on the recent transaction
     Click on report issue button
     Verify support ticket screen


Navigate back to home screen 
    Goto home screen
    
Logout
    Logout of the application

