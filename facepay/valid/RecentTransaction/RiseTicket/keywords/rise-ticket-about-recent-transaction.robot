*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Variables         ../../../var/variables.py



*** Keywords ***
Click on the recent transaction
    Click Element     xpath=//*[@text=\"Inqude LLC\"]
    Sleep    5s

    
Click on report issue button
    Click Element     xpath=//*[@text=\"Report issue\"]
    Sleep    2s
    
Verify support ticket screen
    Page Should Contain Text       ${createSupportTicker}     
    
    # Choose issue type label is displaying
    ${issueTypeLabel}=  Run Keyword And Return Status    Element Should Be Visible   xpath=//*[@text=\"Choose issue type\"]
    Log     ${issueTypeLabel}    

    # Choose issue dropdown is displaying
    ${issueTypeDropdown}=  Run Keyword And Return Status    Element Should Be Visible   xpath=//*[@text=\"I didn't do the payment\"]
    Log     ${issueTypeDropdown}      
    
    # Choose issue summary label is displaying
    ${issueTypeSummary}=  Run Keyword And Return Status    Element Should Be Visible   xpath=//*[@text=\"Issue summary\"]
    Log     ${issueTypeSummary}        
    Sleep    2s
    
    
    # enter issue summary
    Input Text   accessibility_id=create_tiket_summary      ${issueDescription} 
    Sleep    2s
    
    # click on report button
    Click Element    xpath=//*[@text=\"Report\"]        
    Sleep    5s
    
    
Goto home screen
      Back to Home Screen
    
    
    
    
    
    