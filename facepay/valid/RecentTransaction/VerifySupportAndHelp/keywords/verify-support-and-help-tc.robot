*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Variables         ../../../var/variables.py
Resource          ../../../BaseModules/menuButton.robot



*** Keywords ***

Click on Support Ticket
    Sleep    2s    
    Click Element    xpath=//*[@class=\'android.view.ViewGroup\'][@index=1]   
    Sleep    2s
    
Support&Help Ticket Verification
    # screen name validation
    Page Should Contain Text       ${VSTPageTitle} 
    
    # ticket number is displaying
    ${ticketNumber}=  Run Keyword And Return Status    Element Should Be Visible   accessibility_id=ticket_number
    Log     ${ticketNumber} 
    
    # ticket date is displaying
    ${ticketDate}=  Run Keyword And Return Status    Element Should Be Visible   accessibility_id=ticket_date
    Log     ${ticketDate}    
  
    
    # issue type is displaying
    ${issueType}=  Run Keyword And Return Status    Element Should Be Visible   accessibility_id=ticket_issue_type
    Log     ${issueType}    
    
    # issue summary is displaying
    ${issueSummary}=  Run Keyword And Return Status    Element Should Be Visible   accessibility_id=ticket_issue_summary
    Log     ${issueSummary}    
    
    # Comparing wheather the same issue has been raised 
    ${issueSummaryContent}    Get Text     accessibility_id=ticket_issue_summary
    Should be equal as strings    ${issueSummaryContent}    ${issueDescription}
    Sleep    2s   
    
    
Navigate back to support and help screen 
    Click Element    xpath=//*[@class='android.widget.ImageButton'][@index=0] 
    Sleep    3s
 
Navigate back to home screen   
    Click Element    xpath=//*[@class='android.widget.ImageButton'][@index=0]      
    Sleep    1s