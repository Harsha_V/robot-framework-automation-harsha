*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 


Resource          ../../BaseModules/base-setup.robot
Resource          ../../BaseModules/pattern.robot
Resource          ../../BaseModules/menuButton.robot
Resource          ../../BaseModules/login.robot
Resource          ../../BaseModules/logout.robot
Resource          keywords/verify-support-and-help-tc.robot




*** Test Cases ***
Launching FacePay Application     
    Launch FacePay Application
    

Login shall Pass
    Login Should Pass
    
    
Swipe pattern lock 
    Swipe pattern lock and continue  
    
    
Click on Support&Help Button
    Support & Help Button
 
 
Verify ticket raised in Supprt & Help
    Click on Support Ticket
    Support&Help Ticket Verification
    Navigate back to support and help screen  
    Navigate back to home screen   
    
   
Logout   
    Logout of the application






    
    