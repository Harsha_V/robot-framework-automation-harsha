*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 


Resource          ../../BaseModules/base-setup.robot
Resource          ../../BaseModules/pattern.robot
Resource          ../../BaseModules/menuButton.robot
Resource          ../../BaseModules/login.robot
Resource          ../../BaseModules/logout.robot
Resource          keywords/click-all-filters-tc.robot



*** Test Cases ***
Launching FacePay Application     
    Launch FacePay Application
    

Login shall Pass
    Login Should Pass
    
    
Swipe pattern lock 
    Swipe pattern lock and continue  
    
Click on more button in Recent Transactions
    More button
    
Click on All the three Filters 
    This week
    This month
    Last 3 months
    
Select using Calender filter
    Click on calender icon    
    
    
Select start date and end date 
    Select start date
    Select end date    
    

Goto home screen
     Back to Home Screen
    
    
Logout 
    Logout of the application






























