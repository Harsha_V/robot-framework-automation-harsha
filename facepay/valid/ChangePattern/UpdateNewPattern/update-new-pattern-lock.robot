*** Settings ***
Library  AppiumLibrary  20
Library    String    
Library    OperatingSystem 


Resource          ../../BaseModules/base-setup.robot
Resource          ../../BaseModules/pattern.robot
Resource          ../../BaseModules/menuButton.robot
Resource          ../../BaseModules/login.robot
Resource          ../../BaseModules/logout.robot
Resource          keywords/connect-three-dots.robot



*** Test Cases ***
Launching FacePay Application     
    Launch FacePay Application
    

Login shall Pass
    Login Should Pass
    
    
Swipe pattern lock 
    Swipe pattern lock and continue  
    

Click on change pattern
    Change Pattern Button
    
Verify Change Pattern Screen Heading
    Pattern heading 
    
Draw old pattern
    Old pattern
    
    
Draw New Pattern Lock Again
    Draw new pattern lock
    
Click on Continue Button
    Continue button
    
# Replace Old Pattern    
Click on change pattern
    Change Pattern Button
    
Verify Change Pattern Screen Heading
    Pattern heading 
    
Draw old pattern
    Enter latest pattern lock
    
    
Draw New Pattern Lock Again
    Replace to old pattern lock
    
Click on Continue Button
    Continue button
Logout 
    Logout of the application
    
    
    
    
    
    
