*** Settings ***
Library  AppiumLibrary  20
Library    String    
Library    OperatingSystem 

Variables         ../../../var/variables.py
Resource          ../../../BaseModules/menuButton.robot


*** Keywords ***
    
Pattern heading
    Sleep    1s
    ${heading1}     Get Text    xpath=//*[@text=\"Please enter your existing pattern\"]   
    Should Not Be Equal     ${heading1}        ${ChangePatternHeading1}    
   
   
Old pattern
    Swipe     160     535     620      600
    Sleep    2s
    
    Sleep    1s
    ${newPattern}     Get Text    xpath=//*[@text=\"Draw a new unlock pattern\"]   
    Should Not Be Equal     ${newPattern}        ${NewUnlockPattern}    
    Sleep    1s
    
    
    
Enter latest pattern lock
    Swipe     150     745     620      800
    Sleep    2s
    
    
Replace to old pattern lock
    Swipe     160     535     620      600
    Sleep    2s

    
    
   
Draw new pattern lock
    Swipe     150     745     620      800
    Sleep    2s
    
    
    
Continue button 
    Click Element    xpath=//*[@text=\"Continue\"]
    Sleep    2s
    Wait Until Page Contains    Recent Transactions

    
   
