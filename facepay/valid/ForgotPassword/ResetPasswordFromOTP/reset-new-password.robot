*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Resource          ../../BaseModules/base-setup.robot
Resource          ../../BaseModules/pattern.robot
Resource          ../../BaseModules/menuButton.robot
Resource          ../../BaseModules/login.robot
Resource          ../../BaseModules/logout.robot
Resource          keywords/reset-new-password-tc.robot

*** Test Cases ***
Launching FacePay Application     
    Launch FacePay Application
    
Click on forgot password
    ForgotPin Button
     
  
Enter Email Address
    Email Address
        
    
Click on Reset Password
    Reset Password Button
    
Run FacePay application in Background
    Put FacePay application in Background
    
Open message Application
    Message application  
    
Read and Enter the OTP received from the registered mobile number
    Enter OTP received through message    

Enter new password 
    New Password 
    
Enter confirm new password 
    Confirm New Password
    
Click on Save Password Button
    Save BUtton
    
    
# Reset to old password   
    
Click on forgot password
    ForgotPin Button
     
  
Enter Email Address
    Email Address 
    
Click on Reset Password
    Reset Password Button
    
Run FacePay application in Background
    Put FacePay application in Background
    
Open message Application
    Message application  
    
Read and Enter the OTP received from the registered mobile number
    Enter OTP received through message    

Enter new password 
    Old Password 
    
Enter confirm new password 
    Confirm Old Password
    
Click on Save Password Button
    Save BUtton    





