*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Variables         ../../../var/variables.py



*** Keywords ***
ForgotPin Button
    Sleep    5s
    Click Element     xpath=//*[@text=\"Forgot password\"]
    Wait Until Page Contains    Forgot password
    
Email Address
    Input Text   accessibility_id=forgotPassword_email            ${Email} 
    
Reset Password Button
    Sleep    2s
    Click Element     xpath=//*[@text=\"Reset Password\"]
    Wait Until Page Contains    Reset Password
    Sleep    3s
    
Put FacePay application in Background
    Background App    3
    
    
Message application 
    Start Activity    com.google.android.apps.messaging    com.google.android.apps.messaging.ui.ConversationListActivity                    
    Sleep    5s
    
    
Enter OTP received through message
    Wait Until Page Contains    FacePay
    ${subjectLine}     Get Text    id=com.google.android.apps.messaging:id/conversation_snippet
    Log     ${subjectLine}    
    
    # Verify OTP received
    Should Contain    ${subjectLine}    FacePay
    
    # otp from email
    @{otp}     Split String   ${subjectLine}     
    Log    @{otp}[0] 
    
    # Switch back to FacePay app
    Press Keycode    187
    Sleep    1s
    Click Element    accessibility_id=FacePay    
     
    Sleep    2s
    
    # Verify switched to Facepay app
    Page Should Contain Text    Reset password
    
    #Enter the OTP
    Input Text   accessibility_id=reset_otp_code            @{otp}[0]    
    
    
New Password          
    Input Text   accessibility_id=reset_password            ${NewPwd}
    
Confirm New Password          
    Input Text   accessibility_id=reset_confirmPassword            ${NewPwd}
    
    
Old Password          
    Input Text   accessibility_id=reset_password            ${NewPassword}
    
Confirm Old Password          
    Input Text   accessibility_id=reset_confirmPassword            ${NewPassword}
    
    
Save BUtton
    Click Element    xpath=//*[@text=\"Save password\"]
    Wait Until Page Contains       Welcome to FacePay!   
    Sleep    1s
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    