*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Variables         ../../../var/variables.py
Resource          ../../../BaseModules/menuButton.robot


*** Keywords ***
    
Make fourth card from top as default  
    Click Element    xpath=//*[@class='android.view.ViewGroup'][@index=3]
    Wait Until Page Contains      Edit Card 
    Click Element    accessibility_id=toggle_default
    Click Element    xpath=//*[@text=\"Save\"]
    Wait Until Page Contains       Cards management
    

Goto home screen
    Back to Home Screen
