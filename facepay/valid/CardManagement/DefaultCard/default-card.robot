*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 


Resource          ../../BaseModules/base-setup.robot
Resource          ../../BaseModules/pattern.robot
Resource          ../../BaseModules/menuButton.robot
Resource          ../../BaseModules/login.robot
Resource          ../../BaseModules/logout.robot
Resource          keywords/make-card-as-default.robot



*** Test Cases ***
Launching FacePay Application     
    Launch FacePay Application
    

Login shall Pass
    Login Should Pass
    
    
Swipe pattern lock 
    Swipe pattern lock and continue  
        

Click on cards management
    Cards Management Button
    

Make the existing card as default
    Make fourth card from top as default 


Back to Home Screen
     Goto home screen
    
    
Logout 
    Logout of the application

