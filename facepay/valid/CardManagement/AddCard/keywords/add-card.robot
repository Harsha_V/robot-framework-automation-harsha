*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Variables         ../../../var/variables.py
Resource          ../../../BaseModules/menuButton.robot


*** Keywords ***

Add Card    
    Click Element    xpath=//*[@text=\"Add Card\"]
    Wait Until Page Contains      Card   
    
    Input Text       xpath=//*[@text=\"424242424242\"]           ${CardNumber}
    Sleep    1s 
    Input Text       xpath=//*[@text=\"04/24\"]                  ${CardValidity}
    Sleep    1s
    Input Text       xpath=//*[@text=\"123\"]                    ${CardCVV}
    Sleep    1s 
    
    Click Element    xpath=//*[@text=\"Save\"]
    Wait Until Page Contains       Cards management  
    Sleep    3s 
    
Goto home screen
    Back to Home Screen
    
