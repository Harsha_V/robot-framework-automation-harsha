*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Variables         ../../../var/variables.py
Resource          ../../../BaseModules/menuButton.robot


*** Keywords ***
    
    
Delete third Card from top    
    Click Element    xpath=//*[@class='android.view.ViewGroup'][@index=2]
    Wait Until Page Contains      Edit Card 
    Click Element    xpath=//*[@text=\"Delete Card\"]
    Wait Until Page Contains       Cards management 
    
goto home screen
    Back to Home Screen

